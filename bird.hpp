///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   21_FEB_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include<string>

#include "animal.hpp"

namespace animalfarm{
      
      class Bird : public Animal {
         public:
             enum Color featherColor;
             bool       isMigratory;

             void printInfo();
             virtual const string speak();
      };

}//namespace animalfarm
