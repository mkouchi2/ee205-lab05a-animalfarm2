///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Matthew Kouchi <mkouchi2@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   21_FEB_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float newWeight, enum Color newColor, enum Gender newGender ){
      weight = newWeight;
      scaleColor = newColor;
      gender = newGender;
      favoriteTemp = 75;
      species = "Katsuwonus pelamis";
}

void Aku::printInfo(){
     cout << "Aku" << endl;
     cout << "   Weight = [" << weight << "]" << endl;
     Fish::printInfo();

}

}//namespace animalfarm
